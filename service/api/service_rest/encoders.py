from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment



class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]



class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold"
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "customer",
        "technician",
        "vin",
        "id"
    ]

    encoders = {
        "technician": TechnicianEncoder(),
    }

    def get_extra_data(self, o):
        try:
            automobile = AutomobileVO.objects.get(vin=o.vin)
            sold_status = automobile.sold
            print(f"fectched data automobile: {automobile}, sold status: {sold_status}")
        except AutomobileVO.DoesNotExist:
            sold_status = False

        is_vip = sold_status is True

        date = o.date_time.strftime('%Y-%m-%d')
        time = o.date_time.strftime('%H:%M:%S')

        return {
            "vin": o.vin,
            "sold": sold_status,
            "is_vip": is_vip,
            "date": date,
            "time": time
        }
