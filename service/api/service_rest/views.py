from django.http import JsonResponse
from django.utils import timezone
from django.views.decorators.http import require_http_methods
import json
from .models import Technician, Appointment
from .encoders import AppointmentEncoder, TechnicianEncoder



@require_http_methods(["GET", "POST"])
def api_list_technicians(request):

    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianEncoder,
            safe=False
        )
    
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create technician"}
            )
            response.status_code = 400
            return response
    

@require_http_methods(["GET", "DELETE", "PUT"])
def api_detail_technician(request, pk):


    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "Technician does not exist"}
            )
            response.status_code = 404
            return response
    
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"}
            )
    
    else:
        
        content = json.loads(request.body)
        try: 
            if "technician" in content:
                technician = Technician.objects.get(id=pk)
                content["technician"] = technician

        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
        
        Technician.objects.filter(id=pk).update(**content)
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )
        


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False
        )
    
    else:
        try:
            content = json.loads(request.body)

            datetime = content.get("date_time")
            if datetime:
                aware_datetime = timezone.datetime.fromisoformat(datetime)
                content["date_time"] = aware_datetime

            tech_id = content.pop("technician", None)
            if tech_id is not None:
                try:
                    technician = Technician.objects.get(id=tech_id)
                except Technician.DoesNotExist:
                    return JsonResponse(
                        {"message": "Tech not found"},
                        status=404
                    )
                content["technician"] = technician
            
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        
        except:
            response = JsonResponse(
                {"message": "Could not create appointment"}
            )
            response.status_code = 400
            return response
        

@require_http_methods(["GET", "DELETE", "PUT"])
def api_detail_appointments(request, pk):

    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                {"message": "Appointment Deleted"}
            )
        
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"})
    
    else:
        content = json.loads(request.body)
        try:
            Appointment.objects.filter(id=pk).update(**content)
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "appointment does not exist"},
                status=404
            )

@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.status = "cancelled"
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "appointment does not exist"}
            )
        
@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    if request.method == "PUT":
        try: 
            appointment = Appointment.objects.get(id=pk)
            appointment.status = "finished"
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"}
            )
