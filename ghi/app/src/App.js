import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './Inventory/ManufacturerList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import ModelList from './Inventory/ModelList';
import ModelForm from './Inventory/ModelForm';
import AutomobileList from './Inventory/AutomobileList';
import AutomobileForm from './Inventory/AutomobileForm';
import TechniciansList from './Service/TechnicianList'
import TechnicianForm from './Service/TechnicianForm';
import AppointmentsList from './Service/AppointmentList';
import AppointmentForm from './Service/AppointmentForm';
import ServiceHistory from './Service/ServiceHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="Manufacturers">
          <Route index element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>

          <Route path="models">
          <Route index element={<ModelList />} />
            <Route path="new" element={<ModelForm />} />
          </Route>

          <Route path="Automobiles">
            <Route index element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>

          {/* <Route path="salespeople">
            <Route index element={<SalesPeopleList />} />
            <Route path="new" element={<SalesPersonForm />} />
          </Route>

          <Route path="customer">
            <Route index element={<CustomerList />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>

          <Route path="sales">
            <Route index element={<SalesList />} />
            <Route path="new" element={<SalesForm />} />
          </Route>

          <Route path="salespersonhistory">
            <Route index element={<SalespersonHistory />} />
          </Route> */}

          <Route path="technicians">
            <Route index element={<TechniciansList />} />
            <Route path="new" element={<TechnicianForm />} />
          </Route>

          <Route path="appointments">
            <Route index element={<AppointmentsList />} />
            <Route path="new" element={<AppointmentForm />} />
          </Route>

          <Route path="servicehistory">
            <Route index element={<ServiceHistory />} />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
