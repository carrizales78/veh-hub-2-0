import React, { useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom';

function AutoForm() {
    const [autos, setAutos] = useState([]);

    const getAutos = async () => {
        const response = await fetch("http://localhost:8100/api/automobiles/")
    
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    };

    useEffect(() => {
        getAutos();
    }, [])

    const [models, setModels] = useState([])
        const getModels = async () => {
            try {
                const response = await fetch('http://localhost:8100/api/models/');
                if (response.ok) {
                    const data = await response.json();
                    setModels(data.models);
                } else {
                    console.error('Failed to fetch models:', response.statusText);
                }
            } catch (error) {
                console.error('Error fetching models:', error);
            }
        };
        useEffect(() => {
            getModels();
        }, [])


    const [auto, setAuto] = useState({ 
        color: '',
        year: '',
        vin: '',
        model_id: '',
    });

    const handleChange = (event) => {
        const { name, value } = event.target
        if (name === 'model_id') {
            setAuto(prevModel => ({
                ...prevModel,
                [name]: parseInt(value, 10) }));
        } else { 
            setAuto(prevModel => ({...prevModel, [name]: value }));
        }
    };


    const [submitStatus, setSubmitStatus] = useState("");
    const navigate = useNavigate();

    const handleSubmit = async (event) => {
        event.preventDefault();

        const locationUrl = 'http://localhost:8100/api/automobiles/'; 
        const fetchConfig = {
            method: 'POST', // POST method for creating a new entry
            body: JSON.stringify({
                ...auto,
                model_id: auto.model_id, //sends manufacturing_id
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        try {
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                setAuto({
                    color: '',
                    year: '',
                    vin: '',
                    model_id: '',
                });
                setSubmitStatus('Automobile added successfully');
                setTimeout(() => setSubmitStatus(''), 2000);
            } else {
                const errorData = await response.json();
                setSubmitStatus("error: " + errorData.message)

            }
        } catch (error) {
            console.error('Error submitting form', error);
            setSubmitStatus('Submission failed: ' + error.message)
        }
        };
        const navToAutoList = () => {
            navigate('/automobiles/'); 
    };
    

    return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add an automobile to inventory</h1>

                <form onSubmit={handleSubmit} id="create-automobile-form">
                    <div className="form-floating mb-3">
                        <input value={auto.color} onChange={handleChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                        <label htmlFor='color'>Color</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input value={auto.year} onChange={handleChange} placeholder="Year..." required type="text" name="year" id="year" className="form-control" />
                        <label htmlFor='year'>Year...</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input value={auto.vin} onChange={handleChange} placeholder="VIN..." required type="text" name="vin" id="vin" className="form-control" />
                        <label htmlFor='vin'>VIN...</label>
                    </div>

                    <div className="mb-3">
                        <select value={auto.model_id} onChange={handleChange} required name = 'model_id' id='model_id' className='form-select'>
                            <option value=''>Choose a model...</option>
                            {models.map((model) => {
                                return (
                                    <option key={model.id} value={model.id}>
                                        {model.name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>

                    <button type="submit" className="btn btn-primary me-3">Create</button>

                    <button type="button" className="btn btn-secondary " onClick={navToAutoList}>
                    Automobiles </button>


                    {submitStatus && <div className='alert alert-info mt-3'>{submitStatus}</div>}
                </form>
            </div>
        </div>
    </div>
    );
}

export default AutoForm;
