import React, { useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom';

function ModelForm() {
    const [models, setModels] = useState([]);

    const getModels = async () => {
        const response = await fetch("http://localhost:8100/api/models/")
    
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    };

    useEffect(() => {
        getModels();
    }, [])

    const [manufacturers, setManufacturers] = useState([])
        const getManufacturers = async () => {
            try {
                const response = await fetch('http://localhost:8100/api/manufacturers/');
                if (response.ok) {
                    const data = await response.json();
                    setManufacturers(data.manufacturers);
                } else {
                    console.error('Failed to fetch manufacturers:', response.statusText);
                }
            } catch (error) {
                console.error('Error fetching manufacturers:', error);
            }
        };
        useEffect(() => {
            getManufacturers();
        }, [])


    const [model, setModel] = useState({ 
        name: '',
        picture_url: '',
        manufacturer_id: '',
    });

    const handleChange = (event) => {
        const { name, value } = event.target
        if (name === 'manufacturer_id') {
            setModel(prevModel => ({
                ...prevModel,
                [name]: parseInt(value, 10) }));
        } else { 
            setModel(prevModel => ({...prevModel, [name]: value }));
        }
    };


    const [submitStatus, setSubmitStatus] = useState("");
    const navigate = useNavigate();

    const handleSubmit = async (event) => {
        event.preventDefault();

        const locationUrl = 'http://localhost:8100/api/models/'; 
        const fetchConfig = {
            method: 'POST', // POST method for creating a new entry
            body: JSON.stringify({
                ...model,
                manufacturer_id: model.manufacturer_id, //sends manufacturing_id
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        try {
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                setModel({
                    name: '',
                    picture_url: '',
                    manufacturer_id: '',
                });
                setSubmitStatus('Model added successfully');
                setTimeout(() => setSubmitStatus(''), 2000);
            } else {
                const errorData = await response.json();
                setSubmitStatus("error: " + errorData.message)

            }
        } catch (error) {
            console.error('Error submitting form', error);
            setSubmitStatus('Submission failed: ' + error.message)
        }
        };
        const navToModelList = () => {
            navigate('/models/'); 
    };

    return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a vehicle model</h1>

                <form onSubmit={handleSubmit} id="create-model-form">
                    <div className="form-floating mb-3">
                        <input value={model.name} onChange={handleChange} placeholder="Model name" required type="text" name="name" id="name" className="form-control" />
                        <label htmlFor='name'>Model name</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input value={model.picture_url} onChange={handleChange} placeholder="Picture URL..." required type="url" name="picture_url" id="picture_url" className="form-control" />
                        <label htmlFor='name'>Picture URL</label>
                    </div>

                    <div className="mb-3">
                        <select value={model.manufacturer_id} onChange={handleChange} required name = 'manufacturer_id' id='manufacturer' className='form-select'>
                            <option value=''>Choose a manufacturer...</option>
                            {manufacturers.map((manufacturer) => {
                                return (
                                    <option key={manufacturer.id} value={manufacturer.id}>
                                        {manufacturer.name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>

                    <button type="submit" className="btn btn-primary me-3">Create</button>

                    <button type="button" className="btn btn-secondary" onClick={navToModelList}> Models </button>

                    {submitStatus && <div className='alert alert-info mt-3'>{submitStatus}</div>}
                </form>
            </div>
        </div>
    </div>
    );
}

export default ModelForm;
