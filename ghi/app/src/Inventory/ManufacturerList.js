import { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';

function ManufacturersList() {
    const [manufacturers, setManufacturers] = useState([]);
    const navigate = useNavigate();

    const getData = async () => {
        try {
            const response = await fetch('http://localhost:8100/api/manufacturers/');
            if (response.ok) {
                const data = await response.json();
                setManufacturers(data.manufacturers);
            } else {
                console.error('Failed to fetch manufacturers:', response.statusText);
            }
        } catch (error) {
            console.error('Error fetching manufacturers:', error);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    const deleteManufacturers = async (manufacturerId) => {
        try {
            const response = await fetch(`http://localhost:8100/api/manufacturers/${manufacturerId}`, {
                method: "DELETE",
            });

            if (response.ok) {
                getData();
            } else {
                const errorData = await response.json();
                console.error('failed to delete manufacturer:', errorData.message);
            }
        } catch (error) {
            console.error('error deleting manufacturer:', error);
        }
    };

    const navToNewManufacturer = () => {
        navigate('/manufacturers/new'); 
    };

    return (
        <div>    
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => (
                        <tr key={manufacturer.id}>
                            <td>{manufacturer.name}</td>
                            <td>
                                <button 
                                    className="btn btn-danger" 
                                    onClick={() => deleteManufacturers(manufacturer.id)}
                                >
                                    Delete
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
            <button type="button" className="btn btn-primary" onClick={navToNewManufacturer}>
                Add Manufacturer
            </button>
        </div>
    );
}

export default ManufacturersList;
