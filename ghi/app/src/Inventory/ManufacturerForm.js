import React, { useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom';

function ManufacturerForm() {
  const [manufacturers, setManufacturers] = useState([]);

  const getManufacturers = async () => {
    const response = await fetch("http://localhost:8100/api/manufacturers/")
    
    if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
    }
  };

  useEffect(() => {
    getManufacturers();
  }, [])


  const [manufacturer, setManufacturer] = useState({ name: '' });

  const handleChange = (e) => {
    setManufacturer({ ...manufacturer, [e.target.name]: e.target.value });
  }


  const [submitStatus, setSubmitStatus] = useState("");
  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();

    const locationUrl = 'http://localhost:8100/api/manufacturers/'; // Adjusted URL
    const fetchConfig = {
        method: 'POST', // POST method for creating a new entry
        body: JSON.stringify(manufacturer),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    try {
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            setManufacturer({ name: '' });
            setSubmitStatus('Manufacturer added successfully');
            setTimeout(() => setSubmitStatus(''), 2000);
        } else {
            const errorData = await response.json();
            setSubmitStatus("error: " + errorData.message)

        }
    } catch (error) {
        console.error('Error submitting form', error);
        setSubmitStatus('Submission failed: ' + error.message)
    }
    };
    const navToMfrList = () => {
        navigate('/manufacturers/'); 
};

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a manufacturer</h1>

                <form onSubmit={handleSubmit} id="create-manufacturer-form">
                    <div className="form-floating mb-3">
                        <input value={manufacturer.name} onChange={handleChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                        <label htmlFor='name'>Name</label>
                    </div>

                    <button type="submit" className="btn btn-primary me-3">Create</button>

                    <button type="button" className="btn btn-secondary" onClick={navToMfrList}>
                    Manufacturers
                    </button>

                    {submitStatus && <div className='alert alert-info mt-3'>{submitStatus}</div>}
                </form>
              </div>
        </div>
    </div>
    );
}

export default ManufacturerForm;
