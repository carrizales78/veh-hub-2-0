import { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';

function ModelsList() {
    const [models, setModels] = useState([]);
    const navigate = useNavigate();

    const getData = async () => {
        try {
            const response = await fetch('http://localhost:8100/api/models/');
            if (response.ok) {
                const data = await response.json();
                setModels(data.models);
            } else {
                console.error('Failed to fetch models:', response.statusText);
            }
        } catch (error) {
            console.error('Error fetching models:', error);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    const deleteModels = async (modelId) => {
        try {
            const response = await fetch(`http://localhost:8100/api/models/${modelId}`, {
                method: "DELETE",
            });

            if (response.ok) {
                getData();
            } else {
                const errorData = await response.json();
                console.error('failed to delete model:', errorData.message);
            }
        } catch (error) {
            console.error('error deleting model:', error);
        }
    };

    const navToNewModel = () => {
        navigate('/models/new'); 
    };

    return (
        <div>    
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => (
                        <tr key={model.id}>
                            <td>{model.name}</td>
                            <td>{model.manufacturer.name}</td>
                            <td>
                                <img src={model.picture_url} alt={model.name} style={{maxWidth: '100px' }}/>
                            </td>
                            <td>
                                <button 
                                    className="btn btn-danger" 
                                    onClick={() => deleteModels(model.id)}
                                >
                                    Delete
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
            <button type="button" className="btn btn-primary" onClick={navToNewModel}>
                Add Model
            </button>
        </div>
    );
}

export default ModelsList;
