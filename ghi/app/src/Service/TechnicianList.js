import { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';

function TechniciansList() {
    const [technicians, setTechnicians] = useState([]);
    const navigate = useNavigate();

    const getData = async () => {
        try {
            const response = await fetch('http://localhost:8080/api/technicians/');
            if (response.ok) {
                const data = await response.json();
                setTechnicians(data.technician);
            } else {
                console.error('Failed to fetch technicians:', response.statusText);
            }
        } catch (error) {
            console.error('Error fetching manufacturers:', error);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    const deleteTechnicians = async (technicianId) => {
        try {
            const response = await fetch(`http://localhost:8080/api/technicians/${technicianId}`, {
                method: "DELETE",
            });

            if (response.ok) {
                getData();
            } else {
                const errorData = await response.json();
                console.error('failed to delete technician:', errorData.message);
            }
        } catch (error) {
            console.error('error deleting technician:', error);
        }
    };

    const navToNewTechnician = () => {
        navigate('/technicians/new'); 
    };

    return (
        <div>    
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First, Last Name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map(technician => (
                        <tr key={technician.id}>
                            <td>{technician.first_name}, {technician.last_name}</td>
                            <td>{technician.employee_id}</td>
                            <td>
                                <button 
                                    className="btn btn-danger" 
                                    onClick={() => deleteTechnicians(technician.id)}
                                >
                                    Delete
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
            <button type="button" className="btn btn-primary" onClick={navToNewTechnician}>
                Add Technician
            </button>
        </div>
    );
}

export default TechniciansList;
