import { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';

function AppointmentList() {
    const [appointments, setAppointments] = useState([]);
    

    const getData = async () => {
        try {
            const response = await fetch('http://localhost:8080/api/appointments/');
            if (response.ok) {
                const data = await response.json();
                //check for appts field
                if (data.appointments && Array.isArray(data.appointments)) {
                //create array for all other appointments
                    const activeAppointments = [];
                //loop through appointments
                    for (const appointment of data.appointments) {
                //if not cancelled append to array
                        if (appointment.status !== 'cancelled') {
                            activeAppointments.push(appointment);
                        }        
                    }

                //update setAppointments
                    setAppointments(activeAppointments);
                //else if not array set to new array    
                } else {
                    setAppointments([]);
                }

            } else {
                console.error('Failed to fetch Appointments:', response.status)
            }
        } catch (error) {
            console.error("Error fetching appointments:", error)
        }   
    }     


    useEffect(() => {
        getData();
    }, []);

    const [submitStatus, setSubmitStatus] = useState("");
    const navigate = useNavigate();


    const updateAppointmentStatus = async (apptId, action) => {
        let url;

        if (action === 'cancel') {
            url = `http://localhost:8080/api/appointments/${apptId}/cancel/`;
        } else {
            url = `http://localhost:8080/api/appointments/${apptId}/finish/`;
        }

        try {
            const response = await fetch(url, {
                method: "PUT",
            });

            if (response.ok) {
                if (action === 'cancel') {
                    setAppointments(prevAppointments =>
                        prevAppointments.filter(appt => appt.id !== apptId)
                    );
                    setSubmitStatus('appointment canceled');

                } else {
                    getData();
                    setSubmitStatus('appointment finished');
                }

                setTimeout(() => setSubmitStatus(''), 2000);

            } else {
                const errorData = await response.json();
                console.error(`failed to ${action} appointment:`, errorData.message);
            }
        } catch (error) {
            console.error(`error ${action}ing appointment:`, error.message);
        }
    };

    const navToNewAppointment = () => {
        navigate('/appointments/new'); 
    };

    const navToServiceHistory = () => {
        navigate('/servicehistory/')
    }

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>VIP</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        const dateTime = new Date(appointment.date_time)
                        const date = dateTime.toLocaleDateString("en-US")
                        const time = dateTime.toLocaleTimeString("en-US")  
                        const techName = `${appointment.technician.first_name} ${appointment.technician.last_name}`

                        return (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.is_vip ? "Yes" : "No"}</td>
                            <td>{appointment.customer}</td>
                            <td>{date}</td>
                            <td>{time}</td>
                            <td>{techName}</td>
                            <td>{appointment.reason}</td>
                            <td>
                                <button 
                                    className="btn btn-danger" 
                                    onClick={() => updateAppointmentStatus(appointment.id, 'cancel')}
                                >
                                    Cancel
                                </button>
                            </td>
                            <td>
                                <button 
                                    className="btn btn-success" 
                                    onClick={() => updateAppointmentStatus(appointment.id, 'finish')}
                                >
                                    Finish
                                </button>
                            </td>
                        </tr>
                        )
                    })}
                </tbody>
            </table>
            <button type="button" className="btn btn-primary mb-4 me-3" onClick={navToNewAppointment}>
                Add Appointment
            </button>
            <button type="button " className="btn btn-secondary mb-4" onClick={navToServiceHistory}>
                Service History
            </button>

            {submitStatus && <div className='alert alert-info'>{submitStatus}</div>}
        </div>
    );
}

export default AppointmentList;
