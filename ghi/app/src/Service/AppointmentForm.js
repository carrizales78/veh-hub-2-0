import React, { useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom';

function AppointmentForm() {
    const [appointment, setAppointment] = useState({ 
        vin: '',
        customer: '',
        date_time: '',
        technician: '',
        reason: ''
    });

    const [technicians, setTechnicians] = useState([])
        const getTechnicians = async () => {
            try {
                const response = await fetch('http://localhost:8080/api/technicians/');
                if (response.ok) {
                    const data = await response.json();
                    setTechnicians(data.technician);
                } else {
                    console.error('Failed to fetch technicians:', response.statusText);
                }
            } catch (error) {
                console.error('Error fetching technicians:', error);
            }
        };
        useEffect(() => {
            getTechnicians();
        }, [])

    const handleChange = (event) => {
        const { name, value } = event.target
        setAppointment(prevAppointment => ({
            ...prevAppointment,
            [name]: value }));
    };

    const [submitStatus, setSubmitStatus] = useState("");
    const navigate = useNavigate();

    const handleSubmit = async (event) => {
        event.preventDefault();

        const locationUrl = 'http://localhost:8080/api/appointments/'; 
        const fetchConfig = {
            method: 'POST', // POST method for creating a new entry
            body: JSON.stringify({
                ...appointment,
                date: appointment.date,
                time: appointment.time
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        try {
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                setAppointment({
                    vin: '',
                    customer: '',
                    date_time: '',
                    technician: '',
                    reason: ''
                });
                setSubmitStatus('Appointment added successfully');
                setTimeout(() => setSubmitStatus(''), 2000);
            } else {
                const errorData = await response.json();
                setSubmitStatus("error: " + errorData.message)

            }
        } catch (error) {
            console.error('Error submitting form', error);
            setSubmitStatus('Submission failed: ' + error.message)
        }
    };

    const navToApptList = () => {
        navigate('/appointments/'); 
    };

    return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a service appointment</h1>

                <form onSubmit={handleSubmit} id="create-appointment-form">
                    <div className="form-floating mb-3">
                        <input value={appointment.vin} onChange={handleChange} placeholder="Automobile VIN" required type="text" name="vin" id="vin" className="form-control" />
                        <label htmlFor='vin'>Automobile VIN</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input value={appointment.customer} onChange={handleChange} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                        <label htmlFor='customer'>Customer</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input value={appointment.date_time} onChange={handleChange} type="datetime-local" name="date_time" id="date_time" className="form-control" required />
                        <label htmlFor='date_time'>Date and Time</label>
                    </div>

                    <div className="mb-3">
                        <select value={appointment.technician} onChange={handleChange} required name = 'technician' id='technician' className='form-select'>
                            <option value=''>Choose a technician...</option>
                            {technicians.map(technician => {
                                const techName = `${technician.first_name} ${technician.last_name}`
                                return (
                                    <option key={technician.id} value={technician.id}>
                                        {techName}
                                    </option>
                                )

                            })}
                        </select>

                    <div className="form-floating mt-3">
                        <input value={appointment.reason} onChange={handleChange} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                        <label htmlFor='reason'>Reason</label>
                    </div>

                    </div>

                    <button type="submit" className="btn btn-primary me-3">Create</button>
                    <button type="button" className="btn btn-secondary" onClick={navToApptList}>
                    Appointments </button>

                    {submitStatus && <div className='alert alert-info mt-3'>{submitStatus}</div>}

                </form>
            </div>
        </div>
    </div>
    );
}

export default AppointmentForm;
